#include <ESP8266WiFi.h>

char ssid[] = "Livebox-C3E0";
char pass[] = "3X4NcLGcemjvSmQrs3";

String canvas="NULL";

String header = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n";
 
String html_1 = R"=====(
<!DOCTYPE html>
<html>
 <head>
 <meta name='viewport' content='width=device-width, initial-scale=1.0'/>
 <meta charset='utf-8'>
 <meta http-equiv='refresh' content='3'>
 <style>
   body {font-size:100%;} 
   #main {display: table; margin: auto;  padding: 0 10px 0 10px; } 
   h1 {text-align:center; } 
   p { text-align:center; }
 </style>
   <title>lidar mapping</title>
 </head>
 
 <body>
   <div id='main'>
     <h1>Lidar mapping</h1>
     <div id='divcarte'> 
        <canvas id="carte" width="//carteSize//" height="//carteSize//" style="border:1px solid #000000;">
        Your browser does not support the HTML canvas tag.
        </canvas>
     </div>
   </div> 
   <script>
    var c = document.getElementById("carte");
    var ctx = c.getContext("2d");
    //carte//
   </script>
  </body>
</html>
)====="; 
  
WiFiServer server(80);
 
String tmpString;

int serial_reception(){
  int received=0,tmp=0;
  while (!Serial.available()){}
  tmp = Serial.read();
  received = 256 * tmp;
  while (!Serial.available()){}
  tmp = Serial.read();
  received += tmp;
  return received;
}

void setup() {
    ESP.wdtDisable();// software WDT OFF
    *((volatile uint32_t*) 0x60000900) &= ~(1); // Hardware WDT OFF
    Serial.begin(115200);
    
    // Connect to a WiFi network
    //Serial.print(F("Connecting to "));  Serial.println(ssid);
    WiFi.begin(ssid, pass);
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print('0');
        delay(500);
    }
    //Serial.println("");
    //Serial.println(F("[CONNECTED]"));
    //Serial.print("[IP ");              
    //Serial.print(WiFi.localIP()); 
    //Serial.println("]");
 
    // start a server
    server.begin();
    //Serial.println("Server started");
    Serial.print('1');
    tmpString = html_1;
}
 
void loop() {
  WiFiClient client = server.available();
  if(Serial.available()){
    int tmp,x=-1,y=-1,sync=-1;
    
    canvas="ctx.beginPath();";
    bool first = true;
    tmpString = html_1;
    tmp = serial_reception();
    if(tmp==65000){
      Serial.print("start");
      int carteSize=serial_reception();
      tmpString.replace("//carteSize//", String(carteSize));
      while (y!=65001){
        do{
          while(!Serial.available()){}
          sync=Serial.read();
          if(sync !=0){
            Serial.print(sync);
          }
        }while(sync!=0);
        y = serial_reception();
        if(y==65001){break;}
        while(!Serial.available()){}
        x = serial_reception();
        if (first &&(sqrt(x*x+y*y)>carteSize/10)){
          first = false;
          canvas+= "ctx.moveTo("+String(x)+","+String(y)+");";
        }
        else{
          if((sqrt(x*x+y*y)>carteSize/10)){
            canvas +="ctx.lineTo("+String(x)+","+String(y)+");"; 
          }
        }        
      }
      canvas += "ctx.stroke();";
      canvas +="ctx.fillStyle = \"#FF0000\";";
      canvas +="ctx.fillRect("+String(carteSize/2)+","+String(carteSize/2)+", 3, 3);";      
      tmpString.replace("//carte//", canvas );
      Serial.print("end");
      client.flush();
      client.print(header);
      client.print(tmpString);
      delay(100);
    }
  }
  
  else if (client)  {
  client.flush();
  client.print(header);
  client.print(tmpString);
  }
}
