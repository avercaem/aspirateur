//hitachi HLS-LFCD2
//motor : red -> +5v // black -> orange
//serial : red -> +5v // black -> gnd // brown -> RX // green -> TX // blue ???

#define pi 3.14159265
#define RAWDATA_LENGHT 42
#define DATA_NUMBER 360
#define CARTE_SIZE 500 
#define MAX_SIZE 3500 //in mm
 
const int CARTE_DIVIDER=MAX_SIZE/CARTE_SIZE*2;

typedef struct carte_piece{
  int x;
  int y;
 }carte_piece;

carte_piece carte[DATA_NUMBER];

int rawData[RAWDATA_LENGHT];
int data[DATA_NUMBER];
int cptData =0;

void get_data(){
  int getCpt=0;
  while(!Serial1.available()){}
  while (getCpt==0){
    rawData[getCpt] = Serial1.read();
    if(rawData[getCpt]==0xFA){
      getCpt++;
    }
  }
  while (getCpt<42){
    while (Serial1.available()){
      rawData[getCpt++]=Serial1.read();
    }
  }
}

int parse_raw_data(){
  int index=(rawData[1]-160)*6;
  data[index] = rawData[7]*255+rawData[6];
  data[index+1] = rawData[13]*255+rawData[12];
  data[index+2] = rawData[19]*255+rawData[18];
  data[index+3] = rawData[25]*255+rawData[24];
  data[index+4] = rawData[31]*255+rawData[30];
  data[index+5] = rawData[37]*255+rawData[36];
  return index;
}

void parse_data(){
  int x,y;
  for(int i=0;i<DATA_NUMBER;i++){
    y=cos(i*pi/180)*data[i]/CARTE_DIVIDER+CARTE_SIZE/2;
    x=-sin(i*pi/180)*data[i]/CARTE_DIVIDER+CARTE_SIZE/2;
    carte[i].x=x;
    carte[i].y=y;
  }
}

void send_carte(){
  Serial2.write(253);//start sync
  Serial2.write(232);//start sync
  int HCarteSize = CARTE_SIZE/256;
  int LCarteSize = CARTE_SIZE-HCarteSize*256;
  Serial2.write(HCarteSize);
  Serial2.write(LCarteSize);
  for(int i=0;i<DATA_NUMBER;i++){
    Serial2.write(0);
    int x=carte[i].x;
    int y=carte[i].y;  
    int Hx = x/256, Lx = x-Hx*256;
    int Hy = y/256, Ly = y-Hy*256;
    Serial2.write(Hy);
    Serial2.write(Ly);
    Serial2.write(Hx);
    Serial2.write(Lx);
  }
  Serial2.write(0); //end sync
  Serial2.write(253); //end sync
  Serial2.write(233); //end sync
  Serial.println("end transmission");
}

void setup() {
  // initialize both serial ports:
  Serial.begin(230400);
  Serial1.begin(230400);
  Serial2.begin(115200);

  pinMode(13, OUTPUT);
  
 //start lidar sending 'b'
  Serial1.write(0x62);
  
  Serial.println("start"); 
  Serial.println(CARTE_DIVIDER);
  //delay(10000);
}
void loop() { 
  get_data();//récupération des 42 bytes pour chaque mesure de 6°
  int index = parse_raw_data(); //transformation des 42 bytes en données brutes
  Serial.println(index);
  if (cptData++ == 60){ //si toutes les données ont étés chargés
    cptData =0;
    parse_data(); //chargement sur la carte
    send_carte();
    digitalWrite(13, HIGH); 
    delay(1000);
    digitalWrite(13, LOW); 
    delay(1000);
  }  
}

void serialEvent2() {
  char tmp;
  //Serial.print("SERIAL2 :");
  while (Serial2.available()){
    tmp = Serial2.read();
    //Serial.print(tmp);
  }
  Serial.println();
}
